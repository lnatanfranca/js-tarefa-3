const botaoEnviar = document.querySelector('.send-button');
const listaMensagens = document.querySelector(".messages-list")

botaoEnviar.addEventListener('click', function(){
    const msg = document.querySelector('#mensagem').value;
    
    let msgNode = document.createElement('li');
    msgNode.classList.add('mensagem');
    const msgElement = `
        <p class="texto">${msg}</p>
        <input type="button" class="delete-msg" value="Excluir" onclick="excluirMensagem(this)">
    `;
    msgNode.innerHTML = msgElement;

    listaMensagens.appendChild(msgNode);

    document.querySelector('#mensagem').value = "";
})

const excluirMensagem = function(elemento) {
    const pai = elemento.parentElement;
    pai.remove();
}